<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nowoto Ramen Resto</title>

    <!-- Bootstrap Core CSS -->
    <link href="../5/startbootstrap-half-slider-gh-pages/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../5/startbootstrap-half-slider-gh-pages/css/half-slider.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">NOWOTO RAMEN RESTO </img></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
 <a href="index.php">Home</a>
                    </li>
                  <li>
 <a href="login/login.php"><span class="glyphicon glyphicon-user">Login</a></span>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        
        <!-- /.container -->
    </nav>
    
   
    <!-- Half Page Image Background Carousel Header -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides --><!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span></a>
        <div class="carousel-inner">
          <div class="item active">
            <!-- Set the first background image using inline CSS below. -->
            <div class="fill" style="background-image:url('gambar/ramen_enak.jpg');"></div>
            <div class="carousel-caption"></div>
          </div>
          <div class="item">
            <!-- Set the second background image using inline CSS below. -->
            <div class="fill" style="background-image:url('gambar/ramen4.png');"></div>
            <div class="carousel-caption"></div>
          </div>
          <div class="item">
            <!-- Set the third background image using inline CSS below. -->
            <div class="fill" style="background-image:url('gambar/minuman%20new.png');"></div>
            <div class="carousel-caption"></div>
          </div>
        </div>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
        </a>

    </header>





<!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1>Selamat Datang</h1>
          <hr>
          <p>&nbsp;</p>
          <h4>Halaman web ini di pergunakan untuk pengelolaan data untuk bagian admin, kasir dan dapur pada Nowoto Ramen Resto. Silahkan Login terlebih dahulu untuk memasuki halaman selanjutnya</h4>
        </div>
      </div>
      <!-- Footer -->
      <footer>
        <p><center><img src="gambar/WhatsApp Image 2017-05-14 at 17.50.41 (6).jpeg" width="968" height="724" longdesc="gambar/WhatsApp Image 2017-05-14 at 17.50.41 (10).jpeg"></center></p>
        <hr>
        <p><center><img src="gambar/WhatsApp Image 2017-05-14 at 17.50.41 (10).jpeg" width="968" height="724" longdesc="gambar/WhatsApp Image 2017-05-14 at 17.50.41 (10).jpeg"></center></p>
      </footer>
   	  <center>
      </center>
      	  </div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
     <div class="btn-danger">
       <h4><span class="btn-info"><center>
         <p>&nbsp;</p>
         <p class="active">NOWOTO RAMEN RESTO &copy; YESI LESTARI 2017</p>
         <p>&nbsp;</p>
       </center>
       </span> </h4>
</div>
      </center>
    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="../5/startbootstrap-half-slider-gh-pages/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../5/startbootstrap-half-slider-gh-pages/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
